"""# Grayscale - level1
for bin_count in 16 32 64 128 256;do
    python3 extract_histogram.py ${bin_count} 1 0 ../outputs/out_c0_l1_b${bin_count}.dat;
done

# 3D RGB - level1
for bin_count in 4 8 16 32 64;do
    python3 extract_histogram.py ${bin_count} 1 1 ../outputs/out_c1_l1_b${bin_count}.dat;
done

# Gradient - level1
for bin_count in 90 180 360;do
    python3 extract_histogram.py ${bin_count} 1 2 ../outputs/out_c2_l1_b${bin_count}.dat;
done"""

# Gradient - level1 - bin count : 15 30 45 60
for bin_count in 15 30 45 60;do
    python3 extract_histogram.py ${bin_count} 1 2 ../outputs/out_c2_l1_b${bin_count}.dat;
done

# Grayscale - level2 - bin count : 16
python3 extract_histogram.py 16 2 0 ../outputs/out_c0_l2_b16.dat;
# Grayscale - level3 - bin count16
python3 extract_histogram.py 16 3 0 ../outputs/out_c0_l3_b16.dat;

# 3D RGB - level2 - bin count : 16
python3 extract_histogram.py 16 2 1 ../outputs/out_c1_l2_b16.dat;
# 3D RGB - level3 - bin count : 16
python3 extract_histogram.py 16 3 1 ../outputs/out_c1_l3_b16.dat;

# Gradient - level2 - bin count : 180
python3 extract_histogram.py 180 2 2 ../outputs/out_c2_l2_b180.dat;
# Gradient - level3 - bin count : 180
python3 extract_histogram.py 180 3 2 ../outputs/out_c2_l3_b180.dat;


