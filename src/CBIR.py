import sys
#sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')

import cv2
import numpy as np
import os
from matplotlib import pyplot as plt
import math
import scipy.signal


cache_histogram_dir = "../histogram_cache"
cache_histogram_file_name = "histogram.npy"

real_images_dir = "../dataset"
images_text_dir = "images.dat"
validation_images_text_dir = "validation_queries.dat"
validation_gt_text_dir = "validation_gt.dat"


channel_map = {
    0 : "grayscale",
    1 : "RGB",
    2 : "gradient"
}

""" Converts radian to angle """
def radian2degree(angle):
    angle = np.rad2deg(angle)
    if(angle < 0):
        angle += 360
    return angle

""" Histogram of an gradient of an image """
def gradient_histogram(image , bin_count , level):
    filterx = np.array([[-1,0,1],
                        [-1,0,1],
                        [-1,0,1]])
    filtery = np.array([[1,1,1],
                        [0,0,0],
                        [-1,-1,-1]])
    # Take gradient of x and y of the image
    gradientx = scipy.signal.convolve2d(image, filterx,
    mode="same", boundary="fill",fillvalue=0)
    gradienty = scipy.signal.convolve2d(image, filtery,
    mode="same", boundary="fill", fillvalue=0)

    assert(gradientx.shape[0] == gradienty.shape[0])
    assert(gradientx.shape[1] == gradienty.shape[1])

    height, width = gradientx.shape
    histograms = []
    grid_count = 2 ** (level - 1)
    height_step, width_step = int(height / grid_count), int(width / grid_count)
    for w in range(0, grid_count):
        for h in range(0, grid_count):
            base_h, base_w = h * height_step , w * width_step
            croppedx = gradientx[base_h : base_h + height_step,
                                base_w : base_w + width_step].flatten()
            croppedy = gradienty[base_h : base_h + height_step,
                                base_w : base_w + width_step].flatten()
            histograms.append(gradient_histogram_helper(
                                croppedx,croppedy,bin_count))
    return histograms

""" Helper of the gradient_histogram() function """
def gradient_histogram_helper(derivativex, derivativey, bin_count):
    # Extract orientation and magnitude arrays
    orientation = [radian2degree(math.atan2(y, x))
                    for (x,y) in zip(derivativex, derivativey)]
    magnitude = [math.sqrt(y**2 + x**2)
                    for (x,y) in zip(derivativex, derivativey)]

    # Initialize the histogram
    histogram = np.zeros(bin_count)
    for (o, m) in zip(orientation,magnitude):
        histogram[int((o * bin_count) // 360)] += m
    
    return histogram / sum(histogram)
            
""" Histogram of an Grayscale image """
def grayscale_histogram(image, bin_count, level):
    height, width = image.shape
    if(level == 0):
        hist = np.zeros(bin_count)
        for i in range (0, width):
            for j in range(0, height):
                pixel_value = image[j][i]
                index = (pixel_value * bin_count) // 256
                hist[index] += 1

        # Apply l1-normalization
        return hist / sum(hist)
    elif(level > 0):
        histograms = []
        grid_count = 2 ** (level - 1)
        height_step, width_step = int(height / grid_count), int(width / grid_count)

        for w in range(0, grid_count):
            for h in range(0, grid_count):
                base_h, base_w = h * height_step , w * width_step
                cropped = image[base_h : base_h + height_step,
                                base_w : base_w + width_step]
                histograms.append(grayscale_histogram(cropped, bin_count, level=0))
        
        return histograms

""" Histogram of an RGB image """
def rgb_histogram(image, bin_count, level ):
    # Convert to RGB color space
    height, width, _  = image.shape
    if level == 0:
        hist = np.zeros(bin_count ** 3)
        for i in range (0, width):
            for j in range(0, height):
                pixel_value = image[j][i]
                r_index = (pixel_value[0] * bin_count) // 256
                g_index = (pixel_value[1] * bin_count) // 256
                b_index = (pixel_value[2] * bin_count) // 256
                hist[r_index * (bin_count ** 2)
                    + g_index * bin_count
                    + b_index] += 1
        # Apply l1-normalization
        hist = hist / sum(hist)
        return hist
    elif level > 0:
        histograms = []
        grid_count = 2 ** (level - 1)
        height_step, width_step = int(height / grid_count), int(width / grid_count)

        for w in range(0, grid_count):
            for h in range(0, grid_count):
                base_h, base_w = h * height_step , w * width_step
                cropped = image[base_h : base_h + height_step,
                                base_w : base_w + width_step]
                histograms.append(rgb_histogram(cropped, bin_count, level=0))
        
        return histograms

""" Returns the histogram of given image with given configurations """
def get_histogram(image_name , bin_count, level, channel):
    
    image = cv2.imread("{}/{}".format(real_images_dir,image_name))
    # Get the histogram
    if(channel == 0) :
        image = cv2.cvtColor(image , cv2.COLOR_BGR2GRAY)
        histogram = grayscale_histogram(image, bin_count, level)
    elif(channel == 1) : 
        image = cv2.cvtColor(image , cv2.COLOR_BGR2RGB)
        histogram = rgb_histogram(image, bin_count, level)
    elif(channel == 2) :
        image = cv2.cvtColor(image , cv2.COLOR_BGR2GRAY)
        histogram = gradient_histogram(image , bin_count, level)

    return histogram

""" Plots the histogram of the given image with given configurations """
def plot_histogram(histogram) :
    
    bin_count = len(histogram[0])
    width = height = math.sqrt(len(histogram))
    for i in range(0 , len(histogram)):
        plt.subplot(width , height , i  + 1)
        plt.bar(np.arange(bin_count),histogram[i])
    plt.show()

""" Calculates euclidian distance between two histogram """
def calculate_euclidian(histogram1 , histogram2):
    
    if(type(histogram1[0]) == np.ndarray
    and type(histogram2[0]) == np.ndarray):
        assert(len(histogram1) == len(histogram2))
        euclidian = 0
        for i in range(0, len(histogram1)):
            euclidian += calculate_euclidian(histogram1[i],histogram2[i])
        return euclidian
    else:
        return math.sqrt(sum((histogram1 - histogram2) ** 2))

""" Returns the expected histogram cache directory with given configurations """
def get_histogram_cache_dir(image_name, bin_count, level, channel):
    cache_dir = os.path.join(
        cache_histogram_dir,
        image_name.split(".")[0],
        channel_map[channel],
        "level" + str(level),
        "bins" + str(bin_count)
    )
    if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)
    return cache_dir

""" Finds histogram of the image in the cache, calculates and caches otherwise """
def find_histogram(image_name, bin_count, level, channel):
    
    cache_dir = get_histogram_cache_dir(image_name, bin_count, level, channel)
    
    try:
        histogram = np.load("{}/{}".format(cache_dir,cache_histogram_file_name))
        print("{} histogram is found in cache".format(image_name))
    # If file does not exist
    except:       
        histogram = get_histogram(image_name, bin_count, level, channel)
        np.save("{}/{}".format(cache_dir,cache_histogram_file_name),histogram)
        print("{} histogram is saved at {}".format(image_name,cache_dir))
    return histogram
       

""" Finds optimum threshold for the given configuratiions """
def find_optimum_threshold(ground_truth_file, bin_count, level, channel):
    validation_gt_sets = map(map_ground_truth,
    [line.rstrip("\n") for line in open(validation_gt_text_dir,"r")])
    
    min_threshold = 10000.0
    for (image_name, gt_list) in validation_gt_sets:
        for gt_image in gt_list:
            histogram = find_histogram(image_name, bin_count, level, channel)
            euclidian = get_euclidian(histogram, gt_image, bin_count, level, channel)
            if(euclidian < min_threshold):
                min_threshold = euclidian
    
    return min_threshold

""" CBIR implementation """
def CBIR(query_file, output_file, bin_count, level, channel):
    dataset_images = [line.rstrip("\n") for line in open(images_text_dir)]
    hist_dict = {}
    
    for image in dataset_images:
        hist_dict[image] = get_histogram(image, bin_count, level, channel)
        print("Load histogram of the : " + image)
    # Load histograms in the memory
    
    query_images = [line.rstrip("\n") for line in open(query_file)]
    for query_img in query_images :
        histogram1 = hist_dict[query_img]
        results = []
        for dataset_img in dataset_images :
            euc_d = calculate_euclidian(histogram1, hist_dict[dataset_img])
            results.append((euc_d, dataset_img))

        results.sort(key=lambda tuple : tuple[0])
        with open(output_file,"a+") as out_f :
            out_f.write("{}:".format(query_img))
            for result in results:
                # If it is not the last element
                if(result != results[-1]) :
                    out_f.write("{} {} ".format(result[0], result[1]))
                else :
                    out_f.write("{} {}\n".format(result[0], result[1]))

""" Find ground truth images of the given image name """
def map_ground_truth(ground_truth_line):
    image_name, ground_truth_list = ground_truth_line.split(":")
    return(image_name, ground_truth_list.split(" ")[1:])

def main(bin_count, level, channel, output_file):
    CBIR(validation_images_text_dir, output_file, bin_count, level, channel)

if __name__== "__main__" :
    bin_count = int(sys.argv[1])
    level = int(sys.argv[2])
    channel = int(sys.argv[3])
    validation_images_text_dir = sys.argv[4]
    images_text_dir = sys.argv[5]
    real_images_dir = sys.argv[6]
    output_file = sys.argv[7]
    main(bin_count, level, channel, output_file)